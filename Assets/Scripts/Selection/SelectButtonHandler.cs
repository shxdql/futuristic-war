﻿using UnityEngine;
using UnityEngine.UI;

public class SelectButtonHandler : MonoBehaviour
{
    [SerializeField] Text enemyText, stageText;

    StageHandler sh;

    AudioManager am;

    private void Start()
    {
        am = FindObjectOfType<AudioManager>();
        stageText.text = "STAGE 1";
    }

    private void Update()
    {
        sh = FindObjectOfType<StageHandler>();

        enemyText.text = sh.enemy.ToString();

        switch(sh.enemy)
        {
            case 4:
                sh.stage = 1;
                break;
            case 5:
                sh.stage = 2;
                break;
            case 7:
                sh.stage = 3;
                break;
            case 9:
                sh.stage = 4;
                break;
        }
    }

    public void ChoosePlayer()
    {
        am.Play("Click");

        sh.player = true;

        UnityEngine.SceneManagement.SceneManager.LoadScene("GameScene");
    }

    public void ChooseBoss()
    {
        am.Play("Click");

        sh.boss = true;

        UnityEngine.SceneManagement.SceneManager.LoadScene("GameScene");
    }

    public void NextStage()
    {
        am.Play("Click");

        switch (sh.enemy)
        {
            case 4:
                sh.enemy++;
                stageText.text = "STAGE 2";
                break;
            case 5:
                sh.enemy += 2;
                stageText.text = "STAGE 3";
                break;
            case 7:
                sh.enemy += 2;
                stageText.text = "STAGE 4";
                break;
        }
    }

    public void PrevStage()
    {
        am.Play("Click");

        switch (sh.enemy)
        {
            case 5:
                sh.enemy--;
                stageText.text = "STAGE 1";
                break;
            case 7:
                sh.enemy -= 2;
                stageText.text = "STAGE 2";
                break;
            case 9:
                sh.enemy -= 2;
                stageText.text = "STAGE 3";
                break;
        }
    }

    public void BackButton()
    {
        am.Play("Click");

        UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
    }
}
