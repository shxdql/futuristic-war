﻿using UnityEngine;
using UnityEngine.UI;

public class StageHandler : MonoBehaviour
{
    InputField nameInput;

    [HideInInspector] public bool player, boss, obstacleSpawn;

    [HideInInspector] public string names;

    [HideInInspector] public int enemy, stage;

    static StageHandler instance;

    private void Start()
    {
        DontDestroyOnLoad(gameObject);

        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        player = boss = false;

        enemy = 4;
        stage = 1;
    }

    private void Update()
    {
        if (GameObject.Find("NameInput") != null)
            nameInput = GameObject.Find("NameInput").GetComponent<InputField>();

        names = nameInput.text.ToString();
    }
}
