﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuAnimation : MonoBehaviour
{
    [SerializeField] GameObject skippButt;

    public void StartScene()
    {
        SceneManager.LoadScene("SelectionScene");
    }

    public void SettingScene()
    {
        SceneManager.LoadScene("Setting");
    }

    public void HighscoreScene()
    {
        SceneManager.LoadScene("Highscore");
    }

    public void ExitApplication()
    {
        Application.Quit();
    }

    public void DestroySkipButton()
    {
        Destroy(skippButt);
    }
}
