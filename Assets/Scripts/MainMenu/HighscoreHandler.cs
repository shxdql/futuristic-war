﻿using UnityEngine;
using UnityEngine.UI;

public class HighscoreHandler : MonoBehaviour
{
    DatabaseHandler dh;

    AudioManager am;

    [SerializeField] Text nameText1, scoreText1, nameText2, scoreText2, nameText3, scoreText3, nameText4, scoreText4;

    [SerializeField] Animator anim;

    string names1, names2, names3, names4;

    int score1, score2, score3, score4;

    int stage;

    private void Start()
    {
        dh = FindObjectOfType<DatabaseHandler>();
        am = FindObjectOfType<AudioManager>();

        stage = 1;

        LoadDataFromDatabase();

        DisplayHighscore();
    }

    public void NextStage()
    {
        am.Play("Click");

        switch (stage)
        {
            case 1:
                anim.SetTrigger("Stage12");
                stage++;
                break;
            case 2:
                anim.SetTrigger("Stage23");
                stage++;
                break;
            case 3:
                anim.SetTrigger("Stage34");
                stage++;
                break;
        }
    }

    public void PrevStage()
    {
        am.Play("Click");

        switch (stage)
        {
            case 2:
                anim.SetTrigger("Stage21");
                stage--;
                break;
            case 3:
                anim.SetTrigger("Stage32");
                stage--;
                break;
            case 4:
                anim.SetTrigger("Stage43");
                stage--;
                break;
        }
    }

    public void BackButt()
    {
        am.Play("Click");

        anim.SetTrigger("Exit");
    }

    void LoadDataFromDatabase()
    {
        names1 = dh.playerData1.name;
        score1 = dh.playerData1.score;
        names2 = dh.playerData2.name;
        score2 = dh.playerData2.score;
        names3 = dh.playerData3.name;
        score3 = dh.playerData3.score;
        names4 = dh.playerData4.name;
        score4 = dh.playerData4.score;
    }

    void DisplayHighscore()
    {
        nameText1.text = names1;
        scoreText1.text = score1.ToString();
        nameText2.text = names2;
        scoreText2.text = score2.ToString();
        nameText3.text = names3;
        scoreText3.text = score3.ToString();
        nameText4.text = names4;
        scoreText4.text = score4.ToString();
    }
}
