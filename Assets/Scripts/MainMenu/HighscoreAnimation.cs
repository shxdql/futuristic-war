﻿using UnityEngine;
using UnityEngine.UI;

public class HighscoreAnimation : MonoBehaviour
{
    [SerializeField] Text stage;

    public void ExitToMainMenu()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
    }

    public void Stage1()
    {
        stage.text = "STAGE 1";
    }

    public void Stage2()
    {
        stage.text = "STAGE 2";
    }

    public void Stage3()
    {
        stage.text = "STAGE 3";
    }

    public void Stage4()
    {
        stage.text = "STAGE 4";
    }
}
