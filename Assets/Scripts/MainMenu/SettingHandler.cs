﻿using UnityEngine;
using UnityEngine.UI;

public class SettingHandler : MonoBehaviour
{
    [SerializeField] GameObject creditPanel, settingPanel;

    [SerializeField] Slider musicSlider, effectSlider;

    [SerializeField] Animator anim;

    public void BackButton()
    {
        FindObjectOfType<AudioManager>().Play("Click");
        anim.SetTrigger("Exit");
    }

    public void OpenCredit()
    {
        FindObjectOfType<AudioManager>().Play("Click");
        anim.SetTrigger("CreditOn");
    }

    public void CloseCredit()
    {
        FindObjectOfType<AudioManager>().Play("Click");
        anim.SetTrigger("CreditOff");
    }

    public void SetVolumeMusic()
    {
        FindObjectOfType<AudioManager>().SetVolume(musicSlider.value, "Music");
    }

    public void SetVolumeEffect()
    {
        FindObjectOfType<AudioManager>().SetVolume(effectSlider.value, "Effect");
    }
}
