﻿using UnityEngine;

public class MainMenuHandler : MonoBehaviour
{
    [SerializeField] Animator anim;

    AudioManager am;

    private void Start()
    {
        am = FindObjectOfType<AudioManager>();
    }

    public void StartButton()
    {
        am.Play("Click");
        anim.SetTrigger("Start");
    }

    public void SettingButton()
    {
        am.Play("Click");
        anim.SetTrigger("Setting");
    }

    public void HighscoreButton()
    {
        am.Play("Click");
        anim.SetTrigger("Highscore");
    }

    public void ExitButton()
    {
        am.Play("Click");
        anim.SetTrigger("Exit");
    }

    public void SkipAnimation()
    {
        anim.SetTrigger("Skip");
    }
}
