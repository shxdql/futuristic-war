﻿using UnityEngine;
using System;

public class AudioManager : MonoBehaviour
{
    public Sound[] sound;

    public static AudioManager instance;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        foreach (Sound s in sound)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }
    }

    private void Start()
    {
        Play("Theme");
    }

    public void Play(string name)
    {
        Sound s = Array.Find(sound, snd => snd.name == name);
        if (s == null)
            return;
        s.source.Play();
    }

    public void Stop()
    {
        foreach (Sound s in sound)
        {
            if (s.tag == "Music")
                s.source.Stop();
        }
    }

    public void SetVolume(float x, string tag)
    {
        foreach (Sound s in sound)
        {
            if (s.tag == tag)
                s.source.volume = x;
        }
    }
}

[System.Serializable]
public class Sound
{
    public AudioClip clip;

    public string name;
    public string tag;
    [Range(0f, 1f)] public float volume;
    [Range(.1f, 3f)] public float pitch;

    public bool loop;

    [HideInInspector] public AudioSource source;
}