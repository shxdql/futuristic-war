﻿using UnityEngine;
using UnityEngine.UI;
using Wilberforce.FinalVignette;

public class GameHandler : MonoBehaviour
{
    [SerializeField] EnemyHandler eh;

    [SerializeField] ObstacleSpawn os;

    [SerializeField] ClockHandler ch;

    [SerializeField] FinalVignetteCommandBuffer vignette;

    [SerializeField] Text enemyRemainText, hpText;

    [SerializeField] Image hpBar, changeSuitBar;

    [SerializeField] GameObject healthPowerUpPrefab, guidePanel;

    [SerializeField] Transform powerUp;

    [SerializeField] Button changeSuitButt;

    [SerializeField] Animator anim;

    DatabaseHandler dh;

    StageHandler sh;

    AudioManager am;

    Transform[] powerUpChild;

    [HideInInspector] public float health, changeSuitMana, changeSuitTimer;

    [HideInInspector] public int killCount;

    private void Start()
    {
        sh = FindObjectOfType<StageHandler>();
        dh = FindObjectOfType<DatabaseHandler>();
        am = FindObjectOfType<AudioManager>();

        health = 100f;

        CreateHealthPowerUp();

        am.Stop();

        switch (sh.stage)
        {
            case 1:
                am.Play("InGame1");
                break;
            case 2:
                am.Play("InGame2");
                break;
            case 3:
                am.Play("InGame3");
                break;
            case 4:
                am.Play("InGame4");
                break;
        }
    }

    private void Update()
    {
        enemyRemainText.text = eh.enemyTrans.Length.ToString();

        hpText.text = health.ToString();
        hpBar.fillAmount = health / 100f;

        if (eh.enemyTrans.Length == 0)
        {
            int score = ((int)ch.time) * killCount * 100;
            dh.SaveData(sh.stage, sh.names, score);
            Debug.Log("Saved to database. Name: " + sh.names + " Score: " + score + " Stage: " + sh.stage);
            am.Stop();
            am.Play("Theme");
            UnityEngine.SceneManagement.SceneManager.LoadScene("SelectionScene");
        }

        if (ch.time <= 0f)
        {
            am.Stop();
            am.Play("Theme");
            UnityEngine.SceneManagement.SceneManager.LoadScene("SelectionScene");
        }

        if (health <= 0f)
        {
            am.Stop();
            am.Play("Theme");
            UnityEngine.SceneManagement.SceneManager.LoadScene("SelectionScene");
        }

        if (health > 20f)
            vignette.VignetteOuterValueDistance = health / 100f * vignette.VignetteFalloff;

        CheckPowerUp();

        ChangeSuitBar();
    }

    void ChangeSuitBar()
    {
        if (changeSuitMana >= 100f)
        {
            changeSuitMana = 100f;
            changeSuitButt.interactable = true;
        }
        else
            changeSuitButt.interactable = false;

        changeSuitBar.fillAmount = changeSuitMana / 100f;
    }

    void CreateHealthPowerUp()
    {
        Vector3 upPos, downPos, rightPos, leftPos;
        GameObject spawner;

        powerUpChild = new Transform[5];

        for (int i = 0; i < powerUpChild.Length; i++)
        {
            upPos = new Vector3(Random.Range(-20f, 20f), Random.Range(20f, 30f), 0f);
            downPos = new Vector3(Random.Range(-20f, 20f), Random.Range(-20f, -30f), 0f);
            rightPos = new Vector3(Random.Range(20f, 30f), Random.Range(-20f, 20f), 0f);
            leftPos = new Vector3(Random.Range(-20f, -30f), Random.Range(-20f, 20f), 0f);

            for (int j = 0; j < os.startPos.Length; i++)
            {
                if(upPos == os.startPos[j].position || downPos == os.startPos[j].position || rightPos == os.startPos[j].position ||
                    leftPos == os.startPos[j].position)
                {
                    upPos = new Vector3(Random.Range(-20f, 20f), Random.Range(20f, 30f), 0f);
                    downPos = new Vector3(Random.Range(-20f, 20f), Random.Range(-20f, -30f), 0f);
                    rightPos = new Vector3(Random.Range(20f, 30f), Random.Range(-20f, 20f), 0f);
                    leftPos = new Vector3(Random.Range(-20f, -30f), Random.Range(-20f, 20f), 0f);

                    i = 0;
                }
            }

            int x = Random.Range(1, 4);

            switch (x)
            {
                case 1:
                    spawner = Instantiate(healthPowerUpPrefab, upPos, transform.rotation);
                    spawner.AddComponent<PowerUp_Health>();
                    spawner.transform.SetParent(powerUp);
                    powerUpChild[i] = spawner.transform;
                    break;
                case 2:
                    spawner = Instantiate(healthPowerUpPrefab, downPos, transform.rotation);
                    spawner.AddComponent<PowerUp_Health>();
                    spawner.transform.SetParent(powerUp);
                    powerUpChild[i] = spawner.transform;
                    break;
                case 3:
                    spawner = Instantiate(healthPowerUpPrefab, rightPos, transform.rotation);
                    spawner.AddComponent<PowerUp_Health>();
                    spawner.transform.SetParent(powerUp);
                    powerUpChild[i] = spawner.transform;
                    break;
                case 4:
                    spawner = Instantiate(healthPowerUpPrefab, leftPos, transform.rotation);
                    spawner.AddComponent<PowerUp_Health>();
                    spawner.transform.SetParent(powerUp);
                    powerUpChild[i] = spawner.transform;
                    break;
            }
        }
    }

    void CheckPowerUp()
    {
        bool[] isNull = new bool[5];

        for(int i = 0; i < powerUpChild.Length; i++)
        {
            if (powerUpChild[i] == null)
                isNull[i] = true;
        }

        if (isNull[0] && isNull[1] && isNull[2] && isNull[3] && isNull[4])
            CreateHealthPowerUp();
    }

    public void StartGame()
    {
        am.Play("Click");

        FindObjectOfType<GuidePanel>().isStart = true;

        anim.SetTrigger("StartGame");
        guidePanel.SetActive(false);
    }

    public void PauseGame()
    {
        am.Play("Click");
        anim.SetTrigger("PauseOn");
    }

    public void ResumeGame()
    {
        am.Play("Click");
        anim.SetTrigger("PauseOff");
    }

    public void MainMenu()
    {
        am.Play("Click");
        anim.SetTrigger("MainMenu");
    }

    public void ExitGame()
    {
        am.Play("Click");
        anim.SetTrigger("Exit");
    }
}
