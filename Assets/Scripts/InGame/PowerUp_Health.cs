﻿using UnityEngine;

public class PowerUp_Health : MonoBehaviour
{
    GameHandler gh;

    private void Start()
    {
        gh = FindObjectOfType<GameHandler>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Player")
        {
            if (gh.health != 100f)
            {
                if (gh.health < 90f)
                {
                    gh.health += 10f;
                    Destroy(gameObject);
                }
                else
                {
                    gh.health = 100f;
                    Destroy(gameObject);
                }
            }
        }
    }
}
