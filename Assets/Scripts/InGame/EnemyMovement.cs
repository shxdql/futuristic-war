﻿using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    [SerializeField] Transform player;

    [SerializeField] GameHandler gh;

    [SerializeField] ButtonScript bs;

    EnemyHandler eh;

    StageHandler sh;

    Movement m;

    [HideInInspector] public Transform[] child;

    [HideInInspector] public Animator[] anim;

    [HideInInspector] public bool[] isLeft;

    [HideInInspector] public float speed;

    private void Start()
    {
        eh = GetComponent<EnemyHandler>();

        sh = FindObjectOfType<StageHandler>();

        m = player.GetComponent<Movement>();

        anim = new Animator[sh.enemy];

        child = new Transform[sh.enemy];

        isLeft = new bool[sh.enemy];

        for(int i = 0; i < child.Length; i++)
        {
            child[i] = eh.enemyTrans[i];
            anim[i] = child[i].GetComponent<Animator>();
            isLeft[i] = false;
        }

        speed = 3f;
    }

    private void Update()
    {
        for(int i = 0; i < child.Length; i++)
        {
            EnemyMove(i);
        }
    }

    void EnemyMove(int x)
    {
        if (Vector2.Distance(child[x].position, player.position) > 2 && Vector2.Distance(child[x].position, player.position) < 15)
        {
            child[x].position = Vector2.MoveTowards(child[x].position, player.position, speed * Time.unscaledDeltaTime);

            anim[x].SetBool("isWalk", true);

            if (player.position.x < child[x].position.x)
            {
                child[x].rotation = Quaternion.Euler(child[x].rotation.x, 180f, child[x].rotation.z);
                isLeft[x] = true;
            }
            else
            {
                child[x].rotation = Quaternion.Euler(child[x].rotation.x, 0f, child[x].rotation.z);
                isLeft[x] = false;
            }
        }
        else if (Vector2.Distance(child[x].position, player.position) < 2)
        {
            anim[x].SetBool("isWalk", false);

            anim[x].SetTrigger("Attack");
        }
        else
        {
            anim[x].SetBool("isWalk", false);
        }
    }

    public void Attack()
    {
        if (bs.isSuit)
            m.anim.SetTrigger("SuitGetAttacked");
        else
            m.anim.SetTrigger("GetAttacked");

        gh.health -= 10f;
    }
}
