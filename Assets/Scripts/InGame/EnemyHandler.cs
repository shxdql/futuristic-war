﻿using System.Collections;
using UnityEngine;

public class EnemyHandler : MonoBehaviour
{
    [SerializeField] GameObject enemy;

    [SerializeField] Transform startPosPar;

    public Transform[] enemyTrans;

    StageHandler sh;

    [HideInInspector] public Transform[] healthBar;

    [HideInInspector] public Transform[] startPos;

    public float[] health;

    private void Start()
    {
        sh = FindObjectOfType<StageHandler>();

        enemyTrans = new Transform[sh.enemy];
        health = new float[sh.enemy];
        healthBar = new Transform[sh.enemy];
        startPos = new Transform[sh.enemy];

        int x = 0;

        switch (sh.stage)
        {
            case 1:
                x = Random.Range(1, 6);
                break;
            case 2:
                x = Random.Range(1, 5);
                break;
            case 3:
                x = Random.Range(1, 3);
                break;
            case 4:
                x = 1;
                break;
        }

        for(int i = 0; i < startPos.Length; i++)
        {
            startPos[i] = startPosPar.GetChild(i + (x - 1));
        }

        StartCoroutine(SpawnEnemy());
    }

    private void Update()
    {
        for(int i = 0; i < health.Length; i++)
        {
            EnemyMovement em = GetComponent<EnemyMovement>();

            healthBar[i] = enemyTrans[i].transform.Find("Health").transform.Find("Bar");
            healthBar[i].localScale = new Vector3((float)(health[i] / 100f), healthBar[i].localScale.y, healthBar[i].localScale.z);

            if (em.isLeft[i])
            {
                healthBar[i] = enemyTrans[i].transform.Find("Health");
                healthBar[i].rotation = Quaternion.Euler(0f, 0f, 0f);
            }
        }
    }

    IEnumerator SpawnEnemy()
    {
        GameObject spawner;

        for (int i = 0; i < enemyTrans.Length; i++)
        {
            spawner = Instantiate(enemy, startPos[i].position, transform.rotation);
            spawner.layer = 8;
            spawner.transform.SetParent(transform);
            enemyTrans[i] = spawner.transform;
            health[i] = 100f;
        }

        yield return null;
    }
}
