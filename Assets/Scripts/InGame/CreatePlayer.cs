﻿using UnityEngine;

public class CreatePlayer : MonoBehaviour
{
    [SerializeField] GameObject player, boss;

    StageHandler sh;

    [HideInInspector] public float speed;

    [HideInInspector] public bool isPlayer, isBoss;

    private void Start()
    {
        isPlayer = isBoss = false;

        sh = FindObjectOfType<StageHandler>();

        if (sh.player)
        {
            isPlayer = true;
            CreateCharacter(player);
            speed = 5f;
        }
        if (sh.boss)
        {
            isBoss = true;
            CreateCharacter(boss);
            speed = 3f;
        }
    }

    void CreateCharacter(GameObject chr)
    {
        GameObject child = Instantiate(chr, transform.position, transform.rotation);
        child.transform.SetParent(transform);
        sh.player = sh.boss = false;
    }
}
