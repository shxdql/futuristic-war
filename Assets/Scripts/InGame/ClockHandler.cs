﻿using UnityEngine;
using UnityEngine.UI;

public class ClockHandler : MonoBehaviour
{
    StageHandler sh;

    [SerializeField] Text clockText;

    [HideInInspector] public float time;

    private void Start()
    {
        sh = FindObjectOfType<StageHandler>();

        switch(sh.stage)
        {
            case 1:
                time = 100f;
                break;
            case 2:
                time = 120f;
                break;
            case 3:
                time = 140f;
                break;
            case 4:
                time = 150f;
                break;
        }
    }

    private void Update()
    {
        if (FindObjectOfType<GuidePanel>().isStart)
        {
            time -= Time.unscaledDeltaTime;
            int t = (int)time;
            clockText.text = t.ToString();
        }
    }
}
