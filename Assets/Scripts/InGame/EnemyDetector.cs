﻿using UnityEngine;
using UnityEngine.UI;

public class EnemyDetector : MonoBehaviour
{
    [SerializeField] GameObject arrowPrefab, enemyDistPar;

    [SerializeField] EnemyHandler eh;

    [SerializeField] Transform player;

    StageHandler sh;

    [HideInInspector] public Transform[] arrow;

    [HideInInspector] public Text[] enemyName, enemyDistance;

    private void Start()
    {
        sh = FindObjectOfType<StageHandler>();

        arrow = new Transform[sh.enemy];
        enemyName = new Text[sh.enemy];
        enemyDistance = new Text[sh.enemy];

        for(int i = 0; i < enemyName.Length; i++)
        {
            enemyName[i] = enemyDistPar.transform.Find("Name" + (i + 1)).GetComponent<Text>();
            enemyDistance[i] = enemyDistPar.transform.Find("Distance" + (i + 1)).GetComponent<Text>();
        }

        for(int i = 0; i < arrow.Length; i++)
        {
            CreateArrow(i);
        }
    }

    private void Update()
    {
        Vector3 playerPos = new Vector3(player.position.x, player.position.y + 2.5f, player.position.z);
        transform.position = playerPos;

        for(int i = 0; i < arrow.Length; i++)
        {
            DetectEnemy(i);
            ShowDistance(i);
        }
    }

    void CreateArrow(int x)
    {
        GameObject spawner = Instantiate(arrowPrefab, transform.position, transform.rotation);
        spawner.transform.SetParent(transform);
        spawner.GetComponentInChildren<SpriteRenderer>().sortingLayerName = "Up";
        arrow[x] = spawner.transform;
    }

    void DetectEnemy(int x)
    {
        Vector3 enemyPos = new Vector3(eh.enemyTrans[x].position.x, eh.enemyTrans[x].position.y + 2.5f, eh.enemyTrans[x].position.z);
        var dir = enemyPos - transform.position;
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;

        arrow[x].rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    void ShowDistance(int x)
    {
        float distance = Vector2.Distance(eh.enemyTrans[x].position, player.position);

        enemyName[x].text = "ENEMY " + (x + 1);
        enemyDistance[x].text = ((int)distance) + " M";
    }
}
