﻿using UnityEngine;

public class CameraMove : MonoBehaviour
{
    [SerializeField] Transform target;

    private void Update()
    {
        if (Range(target).x > 3f)
            transform.position = new Vector3(target.transform.position.x - 3f, transform.position.y, transform.position.z);
        if (Range(target).x < -3f)
            transform.position = new Vector3(target.transform.position.x + 3f, transform.position.y, transform.position.z);
        if (Range(target).y > -1f)
            transform.position = new Vector3(transform.position.x, target.transform.position.y + 1f, transform.position.z);
        if (Range(target).y < -5f)
            transform.position = new Vector3(transform.position.x, target.transform.position.y + 5f, transform.position.z);
    }

    Vector3 Range(Transform target)
    {
        Vector3 th = target.position - transform.position;
        return th;
    }
}
