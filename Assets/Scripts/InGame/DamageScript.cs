﻿using UnityEngine;

public class DamageScript : MonoBehaviour
{
    int x = 0;

    public void PlayerAttack()
    {
        if (FindObjectOfType<ButtonScript>().att)
        {
            FindObjectOfType<ButtonScript>().att2 = true;
            FindObjectOfType<ButtonScript>().att = false;
        }

        else if (FindObjectOfType<ButtonScript>().att2)
        {
            FindObjectOfType<ButtonScript>().att3 = true;
            FindObjectOfType<ButtonScript>().att2 = false;
        }

        else if (FindObjectOfType<ButtonScript>().att3)
        {
            FindObjectOfType<ButtonScript>().att = true;
            FindObjectOfType<ButtonScript>().att3 = false;
        }

        FindObjectOfType<ButtonScript>().AttackRadius();
    }

    public void EnemyAttack()
    {
        FindObjectOfType<EnemyMovement>().Attack();
    }

    public void SuitIsOn()
    {
        FindObjectOfType<ButtonScript>().isSuit = true;
    }

    public void ChangeSuitSound()
    {
        FindObjectOfType<AudioManager>().Play("ChangeSuit");
    }

    public void PlayerWalk1()
    {
        FindObjectOfType<AudioManager>().Play("PlayerWalk1");
    }

    public void PlayerWalk2()
    {
        FindObjectOfType<AudioManager>().Play("PlayerWalk2");
    }

    public void PlayerHurt1()
    {
        if (x == 0)
        {
            FindObjectOfType<AudioManager>().Play("PlayerHurt1");
            x++;
        }
        else
        {
            FindObjectOfType<AudioManager>().Play("PlayerHurt2");
            x = 0;
        }
    }

    public void PlayerHurt2()
    {
        if (x == 0)
        {
            FindObjectOfType<AudioManager>().Play("BossHurt1");
            x++;
        }
        else
        {
            FindObjectOfType<AudioManager>().Play("BossHurt2");
            x = 0;
        }
    }

    public void EnemyHurt()
    {
        FindObjectOfType<AudioManager>().Play("EnemyHurt");
    }
}
