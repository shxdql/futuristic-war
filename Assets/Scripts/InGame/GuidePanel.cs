﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GuidePanel : MonoBehaviour
{
    [SerializeField] public Button startButt;

    [HideInInspector] public bool isStart;

    float playerSpeed, enemySpeed;

    private void Start()
    {
        isStart = false;

        startButt.gameObject.SetActive(false);
    }

    public IEnumerator GuidePanelOn()
    {
        yield return new WaitForSeconds(2);

        startButt.gameObject.SetActive(true);
    }

    public void PauseOn()
    {
        playerSpeed = FindObjectOfType<Movement>().speed;
        enemySpeed = FindObjectOfType<EnemyMovement>().speed;

        FindObjectOfType<Movement>().speed = 0f;
        FindObjectOfType<EnemyMovement>().speed = 0f;
        FindObjectOfType<GuidePanel>().isStart = false;
    }

    public void PauseOff()
    {
        FindObjectOfType<Movement>().speed = playerSpeed;
        FindObjectOfType<EnemyMovement>().speed = enemySpeed;
        FindObjectOfType<GuidePanel>().isStart = true;
    }

    public void MainMenu()
    {
        FindObjectOfType<AudioManager>().Stop();
        FindObjectOfType<AudioManager>().Play("Theme");
        UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
    }

    public void Exit()
    {
        Application.Quit();
    }
}
