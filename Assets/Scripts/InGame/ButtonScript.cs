﻿using UnityEngine;
using System;
using System.Collections;

public class ButtonScript : MonoBehaviour
{
    [SerializeField] Transform player, attackPoint;

    [SerializeField] LayerMask enemyLayer;

    [SerializeField] EnemyHandler eh;

    [SerializeField] CreatePlayer cp;

    [SerializeField] EnemyDetector ed;

    [SerializeField] EnemyMovement em;

    [SerializeField] GameHandler gh;

    StageHandler sh;

    Movement m;

    [HideInInspector] public static ButtonScript instance;

    [HideInInspector] public bool isSuit;

    [HideInInspector] public bool att, att2, att3;

    public bool canReceivedInput, inputReceived;

    float attackRange;

    private void Awake()
    {
        instance = this;

        canReceivedInput = true;
    }

    private void Start()
    {
        isSuit = false;

        sh = FindObjectOfType<StageHandler>();

        m = player.GetComponent<Movement>();

        att3 = att2 = false;
        att = true;

        if (sh.player)
            attackRange = 1f;
        else if (sh.boss)
            attackRange = 1.5f;

        gh.changeSuitMana = 0f;
    }

    private void Update()
    {
        if (m.anim != null)
        {
            if (m.anim.GetBool("isLeft"))
                attackPoint.position = new Vector3(player.position.x - 1.5f, player.position.y + 3.5f, player.position.z);
            else
                attackPoint.position = new Vector3(player.position.x + 1.5f, player.position.y + 3.5f, player.position.z);
        }

        if (isSuit)
        {
            if (gh.changeSuitTimer > 0f)
            {
                gh.changeSuitTimer -= Time.deltaTime;

                if (sh.player)
                    gh.changeSuitMana = gh.changeSuitTimer * 5f;
                else
                    gh.changeSuitMana = gh.changeSuitTimer * 100f / 15f;
            }

            else
            {
                m.anim.SetTrigger("ChangeStd");

                isSuit = false;
            }
        }
    }

    public void ChangeSuit()
    {
        if (!isSuit)
        {
            FindObjectOfType<AudioManager>().Play("ChangeSuit");
            m.anim.SetTrigger("ChangeSuit");

            if (sh.player)
                gh.changeSuitTimer = 20f;
            else
                gh.changeSuitTimer = 15f;
        }
    }

    public void Attacking()
    {
        if (canReceivedInput)
        {
            inputReceived = true;
            canReceivedInput = false;
        }
    }

    public void InputManager()
    {
        if (!canReceivedInput)
            canReceivedInput = true;
    }

    public void AttackRadius()
    {
        Collider2D[] hitEnemy = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, enemyLayer);

        for(int i = 0; i < hitEnemy.Length; i++)
        {
            for(int j = 0; j < eh.enemyTrans.Length; j++)
            {
                if (eh.enemyTrans[j].position == hitEnemy[i].transform.position)
                {
                    if (cp.isPlayer)
                    {
                        if (isSuit)
                        {
                            if (att3)
                            {
                                eh.health[j] -= 15f;
                                gh.changeSuitMana += 15f;
                            }
                            else
                            {
                                eh.health[j] -= 10f;
                                gh.changeSuitMana += 10f;
                            }
                        }
                        else
                        {
                            if (att3)
                            {
                                eh.health[j] -= 12f;
                                gh.changeSuitMana += 12f;
                            }
                            else
                            {
                                eh.health[j] -= 8f;
                                gh.changeSuitMana += 8f;
                            }
                        }
                    }
                    else if (cp.isBoss)
                    {
                        if (isSuit)
                        {
                            if (att3)
                            {
                                eh.health[j] -= 30f;
                                gh.changeSuitMana += 30f;
                            }
                            else
                            {
                                eh.health[j] -= 25f;
                                gh.changeSuitMana += 25f;
                            }
                        }
                        else
                        {
                            if (att3)
                            {
                                eh.health[j] -= 20f;
                                gh.changeSuitMana += 20f;
                            }
                            else
                            {
                                eh.health[j] -= 17f;
                                gh.changeSuitMana += 17f;
                            }
                        }
                    }

                    em.anim[j].SetTrigger("GetAttacked");
                }

                if (eh.health[j] <= 0f)
                {
                    Destroy(eh.enemyTrans[j].gameObject);
                    Destroy(ed.arrow[j].gameObject);
                    eh.health[j] = 0;

                    RemoveElement(ref eh.enemyTrans, j);
                    RemoveElement(ref eh.health, j);
                    RemoveElement(ref eh.healthBar, j);
                    RemoveElement(ref ed.arrow, j);
                    RemoveElement(ref ed.enemyName, j);
                    RemoveElement(ref ed.enemyDistance, j);
                    RemoveElement(ref em.child, j);
                    RemoveElement(ref em.anim, j);
                    GameHandler gh = FindObjectOfType<GameHandler>();
                    gh.killCount++;
                }
            }
        }

        att = att2 = att3 = false;
    }

    void RemoveElement<T>(ref T[] arr, int index)
    {
        for(int i = index; i < arr.Length - 1; i++)
        {
            arr[i] = arr[i + 1];
        }

        Array.Resize(ref arr, arr.Length - 1);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(attackPoint.position, attackRange);
    }
}
