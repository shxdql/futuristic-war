﻿using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] FixedJoystick fixJoy;

    [SerializeField] ButtonScript bs;

    [HideInInspector] public Animator anim;

    Rigidbody2D rb;

    CreatePlayer cp;

    [HideInInspector] public float speed;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        cp = FindObjectOfType<CreatePlayer>();
    }

    private void Update()
    {
        if (anim == null)
            anim = GetComponentInChildren<Animator>();

        speed = cp.speed;

        Moving();
    }

    void Moving()
    {
        if (bs.isSuit)
            Move(2f);
        else
            Move(1f);

        if (anim.GetFloat("MoveX") < -.2f)
            anim.SetBool("isLeft", true);
        if (anim.GetFloat("MoveX") > 0f)
            anim.SetBool("isLeft", false);

        if (anim.GetBool("isLeft"))
            transform.rotation = Quaternion.Euler(transform.rotation.x, 180f, transform.rotation.z);
        else
            transform.rotation = Quaternion.Euler(transform.rotation.x, 0f, transform.rotation.z);

        anim.SetFloat("MoveX", fixJoy.Horizontal);
        anim.SetFloat("MoveY", fixJoy.Vertical);
    }

    void Move(float s)
    {
        rb.velocity = new Vector2(fixJoy.Horizontal, fixJoy.Vertical) * speed * s;
    }
}
