﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawn : MonoBehaviour
{
    [SerializeField] GameObject[] obstacle;

    [SerializeField] Transform startPosPar;

    [HideInInspector] public Transform[] startPos;

    StageHandler sh;

    int obstacleCount;

    private void Start()
    {
        sh = FindObjectOfType<StageHandler>();

        sh.obstacleSpawn = false;
        obstacleCount = 0;

        int x = 0;

        switch (sh.stage)
        {
            case 1:
                obstacleCount = 6;
                x = Random.Range(1, 7);
                break;
            case 2:
                obstacleCount = 7;
                x = Random.Range(1, 6);
                break;
            case 3:
                obstacleCount = 9;
                x = Random.Range(1, 4);
                break;
            case 4:
                obstacleCount = 12;
                x = 1;
                break;
        }

        startPos = new Transform[obstacleCount];

        for (int i = 0; i < startPos.Length; i++)
        {
            startPos[i] = startPosPar.GetChild(i + (x - 1));
        }

        StartCoroutine(CreateObstacle());
    }

    IEnumerator CreateObstacle()
    {
        GameObject spawner;

        for(int i = 0; i < obstacleCount; i++)
        {
            int x = Random.Range(1, 4);

            spawner = Instantiate(obstacle[x - 1], startPos[i].position, transform.rotation);
            spawner.layer = 11;
            spawner.transform.SetParent(transform);
        }

        sh.obstacleSpawn = true;

        yield return null;
    }
}
