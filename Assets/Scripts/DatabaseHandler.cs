﻿using UnityEngine;
using System.IO;
using System.Collections.Generic;

public class DatabaseHandler : MonoBehaviour
{
    [SerializeField] TextAsset stage1, stage2, stage3, stage4;

    static DatabaseHandler instance;

    public Data playerData, playerData1, playerData2, playerData3, playerData4;

    private void Start()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        OpenData();
    }

    public void SaveData (int stage, string name, int score)
    {
        playerData = new Data();
        playerData.name = name;
        playerData.score = score;

        switch (stage)
        {
            case 1:
                if (playerData1.score > playerData.score)
                    playerData = playerData1;
                break;
            case 2:
                if (playerData2.score > playerData.score)
                    playerData = playerData2;
                break;
            case 3:
                if (playerData3.score > playerData.score)
                    playerData = playerData3;
                break;
            case 4:
                if (playerData4.score > playerData.score)
                    playerData = playerData4;
                break;
        }

        string output = JsonUtility.ToJson(playerData);

        switch (stage)
        {
            case 1:
                File.WriteAllText(Application.dataPath + "/Stage1.json", output);
                break;
            case 2:
                File.WriteAllText(Application.dataPath + "/Stage2.json", output);
                break;
            case 3:
                File.WriteAllText(Application.dataPath + "/Stage3.json", output);
                break;
            case 4:
                File.WriteAllText(Application.dataPath + "/Stage4.json", output);
                break;
        }
    }

    void OpenData()
    {
        playerData1 = new Data();
        playerData2 = new Data();
        playerData3 = new Data();
        playerData4 = new Data();

        playerData1 = JsonUtility.FromJson<Data>(stage1.text);
        playerData2 = JsonUtility.FromJson<Data>(stage2.text);
        playerData3 = JsonUtility.FromJson<Data>(stage3.text);
        playerData4 = JsonUtility.FromJson<Data>(stage4.text);
    }
}

[System.Serializable]
public class Data
{
    public string name;
    public int score;
}