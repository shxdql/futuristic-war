﻿using UnityEngine;

public class PlayerTransition : StateMachineBehaviour
{
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        ButtonScript.instance.canReceivedInput = true;
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (ButtonScript.instance.inputReceived)
        {
            animator.SetTrigger("Attack2");
            ButtonScript.instance.InputManager();
            ButtonScript.instance.inputReceived = false;
        }
    }
}
