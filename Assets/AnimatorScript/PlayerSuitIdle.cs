﻿using UnityEngine;

public class PlayerSuitIdle : StateMachineBehaviour
{
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (ButtonScript.instance.inputReceived)
        {
            animator.SetTrigger("SuitAttack");
            ButtonScript.instance.InputManager();
            ButtonScript.instance.inputReceived = false;
        }
    }
}
