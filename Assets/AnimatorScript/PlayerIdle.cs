﻿using UnityEngine;

public class PlayerIdle : StateMachineBehaviour
{
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if(ButtonScript.instance != null)
        {
            if (ButtonScript.instance.inputReceived)
            {
                animator.SetTrigger("Attack");
                ButtonScript.instance.InputManager();
                ButtonScript.instance.inputReceived = false;
            }
        }
    }
}
